import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase,AngularFireList} from 'angularfire2/database';
import { ContactService } from '../contact.service';
import {Contacts} from '../contact.model';
import {NgForm} from '@angular/forms'
import { element } from '@angular/core/src/render3/instructions';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css'],
  
})
export class ContactListComponent implements OnInit {
  //
  public show:boolean = false;
  public Edit:any = 'Show';
  //
  term;
  contactdata:Contacts[];
  contacts;
  contactlist;
  
  constructor(private contactl:ContactService,private tsr: ToastrService) {
    this.contacts = this.contactl.getcontacts();
}
  
  ngOnInit() {
    this.contactl.getcontacts();
    var x=this.contactl.getcontacts();
    x.snapshotChanges().subscribe(item =>{
    this.contactdata=[];
    item.forEach(element =>{
    var y=element.payload.toJSON();
    y["$key"]=element.key;
    this.contactdata.push(y as Contacts);
    });
    });
 this.resetform();
  }

onSelect(contact:Contacts){
this.contactlist=contact;
}

onSubmit(contactForm:NgForm){
  if(contactForm.value.$key==null){
    this.contactl.insertdata(contactForm.value);
    this.resetform();
  }
  else
  { 
    this.contactl.updateinfo(contactForm.value);
    this.tsr.success('submitted Successfully','Updated');
    this.resetform();
  }
}

//onclick edit button show prefield value in button
onEdit(cnt:Contacts){
this.contactl.selectedcontact=Object.assign({},cnt);
}

toggle(){
  this.show = !this.show;
  if(this.show)  
    this.Edit = "Hide";
  else
    this.Edit = "Show";
}
//Reset form
resetform(contactForm?: NgForm){
if(contactForm!=null)  
contactForm.reset();
}
}
